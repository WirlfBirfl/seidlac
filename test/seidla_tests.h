#ifndef SEIDLA_TESTS
#define SEIDLA_TESTS

char* all_tokenizer_tests();
char* all_primary_expr_parsing_tests();
char *all_token_context_tests();

#endif