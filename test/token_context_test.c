#include "minunit.h"

#include "parser.h"

static char* test_token_context() {
    TokenContext tc;
    token_context_init(&tc);
    mu_assert("byte 0 is not 0", !tc.bitMask[0]);
    mu_assert("byte 1 is not 0", !tc.bitMask[1]);
    mu_assert("byte 2 is not 0", !tc.bitMask[2]);
    mu_assert("byte 3 is not 0", !tc.bitMask[3]);
    mu_assert("byte 4 is not 0", !tc.bitMask[4]);
    mu_assert("byte 5 is not 0", !tc.bitMask[5]);
    mu_assert("byte 6 is not 0", !tc.bitMask[6]);
    mu_assert("byte 7 is not 0", !tc.bitMask[7]);
    mu_assert("byte 8 is not 0", !tc.bitMask[8]);
    mu_assert("byte 9 is not 0", !tc.bitMask[9]);
    mu_assert("byte 10 is not 0", !tc.bitMask[10]);
    mu_assert("byte 11 is not 0", !tc.bitMask[11]);
    mu_assert("byte 12 is not 0", !tc.bitMask[12]);
    mu_assert("byte 13 is not 0", !tc.bitMask[13]);
    mu_assert("byte 14 is not 0", !tc.bitMask[14]);
    mu_assert("byte 15 is not 0", !tc.bitMask[15]);


    int tk[] = {7, 12, 43, 45, 98};
    token_context_add_token(&tc, &tk, 5);
    mu_assert("byte 0 is not 128", tc.bitMask[0] == -128);
    mu_assert("byte 1 is not 16", tc.bitMask[1] == 16);
    mu_assert("byte 5 is not 8", tc.bitMask[5] == 40);
    mu_assert("byte 12 is not 4", tc.bitMask[12] == 4);

    return 0;
}

char *all_token_context_tests() {
    show_testheader("Token context");
    mu_run_test("test token context", test_token_context);
    printf("\n");
    return 0;
}