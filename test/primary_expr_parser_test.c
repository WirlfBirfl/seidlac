#include "minunit.h"
#include <stdio.h>

#include "parsing.h"
#include "tokenizer.h"

static char* assert_token_range(TokenRange *range, size_t expectedStart, size_t expectedEnd) {
    mu_assert("incorrect start index for token range", range->startIndex == expectedStart);
    mu_assert("incorrect end index for token range", range->endIndex == expectedEnd);
    return 0;
}

static char* test_simple_literals() {
    Parser p;
    p.position = 0;
    tokenstream_init(&p.tokens, 1);
    Token trueToken;
    trueToken.kind = TOKEN_KEYWORD_true;
    Token falseToken;
    falseToken.kind = TOKEN_KEYWORD_false;
    Token charToken;
    charToken.kind = TOKEN_LITERAL_CHAR;
    bs_from_c_str(&charToken.strVal, "c", 1);
    Token binIntToken;
    binIntToken.kind = TOKEN_LITERAL_BIN_INT;
    Token hexIntToken;
    hexIntToken.kind = TOKEN_LITERAL_HEX_INT;
    Token intToken;
    intToken.kind = TOKEN_LITERAL_INT;
    Token floatToken;
    floatToken.kind = TOKEN_LITERAL_FLOAT;
    Token stringToken;
    stringToken.kind = TOKEN_LITERAL_STRING;
    Token idToken;
    idToken.kind = TOKEN_ID;

    tokenstream_add(&p.tokens, &trueToken);
    tokenstream_add(&p.tokens, &falseToken);
    tokenstream_add(&p.tokens, &charToken);
    tokenstream_add(&p.tokens, &binIntToken);
    tokenstream_add(&p.tokens, &hexIntToken);
    tokenstream_add(&p.tokens, &intToken);
    tokenstream_add(&p.tokens, &floatToken);
    tokenstream_add(&p.tokens, &stringToken);
    tokenstream_add(&p.tokens, &idToken);

    Expr* result = parse_primary_expr(&p);
    mu_assert("expr kind is not bool literal", result->kind == EXPR_LIT_BOOL);
    mu_assert("expr value is not 1", result->child.boolean);
    assert_token_range(&result->tokens, 0, 0);
    free(result);

    result = parse_primary_expr(&p);
    mu_assert("expr kind is not bool literal", result->kind == EXPR_LIT_BOOL);
    mu_assert("expr value is not 0", !result->child.boolean);
    assert_token_range(&result->tokens, 1, 1);
    free(result);

    result = parse_primary_expr(&p);
    mu_assert("expr kind is not char literal", result->kind == EXPR_LIT_CHAR);
    mu_assert("expr value is not c", result->child.character == 'c');
    assert_token_range(&result->tokens, 2, 2);
    free(result);

    result = parse_primary_expr(&p);
    mu_assert("expr kind is not int literal", result->kind == EXPR_LIT_INT);
    mu_assert("expr value is not bin int", result->child.integer->kind == INT_BIN);
    assert_token_range(&result->tokens, 3, 3);
    free(result);

    result = parse_primary_expr(&p);
    mu_assert("expr kind is not int literal", result->kind == EXPR_LIT_INT);
    mu_assert("expr value is not hex int", result->child.integer->kind == INT_HEX);
    assert_token_range(&result->tokens, 4, 4);
    free(result);

    result = parse_primary_expr(&p);
    mu_assert("expr kind is not int literal", result->kind == EXPR_LIT_INT);
    mu_assert("expr value is not dec int", result->child.integer->kind == INT_DEC);
    assert_token_range(&result->tokens, 5, 5);
    free(result);

    result = parse_primary_expr(&p);
    mu_assert("expr kind is not float literal", result->kind == EXPR_LIT_FLOAT);
    mu_assert("float is not 64 bit wide", result->child.real->bitWidth == 64);
    assert_token_range(&result->tokens, 6, 6);
    free(result);

    result = parse_primary_expr(&p);
    mu_assert("expr kind is not string literal", result->kind == EXPR_LIT_STRING);
    assert_token_range(&result->tokens, 7, 7);
    free(result);

    result = parse_primary_expr(&p);
    mu_assert("expr kind is not id", result->kind == EXPR_ID);
    assert_token_range(&result->tokens, 8, 8);
    free(result);

    return 0;
}

static char* test_compound_literals() {
    Parser p;
    p.position = 0;
    tokenstream_init(&p.tokens, 10);
    Token openBracketToken;
    openBracketToken.kind = TOKEN_OPEN_BRA;
    Token intLiteral;
    intLiteral.kind = TOKEN_LITERAL_INT;
    Token closeBracketToken;
    closeBracketToken.kind = TOKEN_CLOSE_BRA;

    Token openParToken;
    openParToken.kind = TOKEN_OPEN_PAR;
    Token closeParToken;
    closeParToken.kind = TOKEN_CLOSE_PAR;

    // array literal
    tokenstream_add(&p.tokens, &openBracketToken);
    tokenstream_add(&p.tokens, &intLiteral);
    tokenstream_add(&p.tokens, &closeBracketToken);
    // paranthesized expression
    tokenstream_add(&p.tokens, &openParToken);
    tokenstream_add(&p.tokens, &intLiteral);
    tokenstream_add(&p.tokens, &closeParToken);

    Expr *result = parse_primary_expr(&p);
    mu_assert("expr is not an array literal", result->kind == EXPR_ARRAY);
    assert_token_range(&result->tokens, 0, 2);
    free(result);

    result = parse_primary_expr(&p);
    mu_assert("expr is not an int literal", result->kind == EXPR_LIT_INT);
    assert_token_range(&result->tokens, 4, 4);
    free(result);


    return 0;
}

char *all_primary_expr_parsing_tests() {
    show_testheader("Primary expressions");
    mu_run_test("testing simple literals", test_simple_literals);
    mu_run_test("testing compund literals", test_compound_literals);
    printf("\n");
    return 0;
}