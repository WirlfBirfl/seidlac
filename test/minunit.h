#ifndef LEXGEN_MINUNIT_H
#define LEXGEN_MINUNIT_H

#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
#define mu_run_test(msg, test) do {printf("Test number %d:\n", (tests_run + (1)));printf(msg); printf("\n");char *message = test(); printf("\n"); tests_run++; \
                            if (message) return message; } while (0)
#define show_testheader(msg) do {\
    printf("\e[1;36mTesting %s:\n---------------\n\n\e[0m", msg);\
}while(0)

extern int tests_run;

#endif