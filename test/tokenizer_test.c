#include <stdio.h>
#include <string.h>

#include "buffered_string.h"
#include "minunit.h"
#include "tokenizer.h"


static char* test_module_decl_tokens() {
    char test_src[] = "module supermodule;\xFF";
    TokenStream stream = tokenizer_tokenize(test_src);
    mu_assert("Stream size is not 4", stream.length == 4);
    mu_assert("First token is not module token", stream.tokens[0].kind == TOKEN_KEYWORD_module);
    mu_assert("Second token is not id token", stream.tokens[1].kind == TOKEN_ID);
    mu_assert("Module name is not main", bs_cmp_c_str(&stream.tokens[1].strVal, "supermodule") == 0);
    mu_assert("Third token is not semicolon token", stream.tokens[2].kind == TOKEN_SEMICOLON);
    mu_assert("Fourth token is not EOF token", stream.tokens[3].kind == TOKEN_EOF);
    tokenstream_free(&stream);
    return 0;
}

static char* test_get_token_for_keywords() {
    #define KEYWORDS 45
    typedef struct { char *keyword; TokenKind kind;} KeywordTokenPair; 
    KeywordTokenPair keywords[KEYWORDS] = {
        (KeywordTokenPair) {.keyword = "and", .kind = TOKEN_KEYWORD_and },
        (KeywordTokenPair) {.keyword = "asm", .kind = TOKEN_KEYWORD_asm },
        (KeywordTokenPair) {.keyword = "as", .kind = TOKEN_KEYWORD_as },
        (KeywordTokenPair) {.keyword = "Bool", .kind = TOKEN_KEYWORD_Bool },
        (KeywordTokenPair) {.keyword = "break", .kind = TOKEN_KEYWORD_break },
        (KeywordTokenPair) {.keyword = "case", .kind = TOKEN_KEYWORD_case },
        (KeywordTokenPair) {.keyword = "cfn", .kind = TOKEN_KEYWORD_cfn },
        (KeywordTokenPair) {.keyword = "continue", .kind = TOKEN_KEYWORD_continue },
        (KeywordTokenPair) {.keyword = "data", .kind = TOKEN_KEYWORD_data },
        (KeywordTokenPair) {.keyword = "do", .kind = TOKEN_KEYWORD_do },
        (KeywordTokenPair) {.keyword = "elif", .kind = TOKEN_KEYWORD_elif },
        (KeywordTokenPair) {.keyword = "else", .kind = TOKEN_KEYWORD_else },
        (KeywordTokenPair) {.keyword = "error", .kind = TOKEN_KEYWORD_error },
        (KeywordTokenPair) {.keyword = "export", .kind = TOKEN_KEYWORD_export },
        (KeywordTokenPair) {.keyword = "F32", .kind = TOKEN_KEYWORD_F32 },
        (KeywordTokenPair) {.keyword = "F64", .kind = TOKEN_KEYWORD_F64 },
        (KeywordTokenPair) {.keyword = "false", .kind = TOKEN_KEYWORD_false },
        (KeywordTokenPair) {.keyword = "fn", .kind = TOKEN_KEYWORD_fn },
        (KeywordTokenPair) {.keyword = "for", .kind = TOKEN_KEYWORD_for },
        (KeywordTokenPair) {.keyword = "goto", .kind = TOKEN_KEYWORD_goto },
        (KeywordTokenPair) {.keyword = "I8", .kind = TOKEN_KEYWORD_I8 },
        (KeywordTokenPair) {.keyword = "I16", .kind = TOKEN_KEYWORD_I16 },
        (KeywordTokenPair) {.keyword = "I32", .kind = TOKEN_KEYWORD_I32 },
        (KeywordTokenPair) {.keyword = "I64", .kind = TOKEN_KEYWORD_I64 },
        (KeywordTokenPair) {.keyword = "if", .kind = TOKEN_KEYWORD_if },
        (KeywordTokenPair) {.keyword = "import", .kind = TOKEN_KEYWORD_import },
        (KeywordTokenPair) {.keyword = "include", .kind = TOKEN_KEYWORD_include },
        (KeywordTokenPair) {.keyword = "match", .kind = TOKEN_KEYWORD_match },
        (KeywordTokenPair) {.keyword = "module", .kind = TOKEN_KEYWORD_module },
        (KeywordTokenPair) {.keyword = "not", .kind = TOKEN_KEYWORD_not },
        (KeywordTokenPair) {.keyword = "or", .kind = TOKEN_KEYWORD_or },
        (KeywordTokenPair) {.keyword = "return", .kind = TOKEN_KEYWORD_return },
        (KeywordTokenPair) {.keyword = "String", .kind = TOKEN_KEYWORD_String },
        (KeywordTokenPair) {.keyword = "struct", .kind = TOKEN_KEYWORD_struct },
        (KeywordTokenPair) {.keyword = "switch", .kind = TOKEN_KEYWORD_switch },
        (KeywordTokenPair) {.keyword = "syscall", .kind = TOKEN_KEYWORD_syscall },
        (KeywordTokenPair) {.keyword = "true", .kind = TOKEN_KEYWORD_true },
        (KeywordTokenPair) {.keyword = "U8", .kind = TOKEN_KEYWORD_U8 },
        (KeywordTokenPair) {.keyword = "U16", .kind = TOKEN_KEYWORD_U16 },
        (KeywordTokenPair) {.keyword = "U32", .kind = TOKEN_KEYWORD_U32 },
        (KeywordTokenPair) {.keyword = "U64", .kind = TOKEN_KEYWORD_U64 },
        (KeywordTokenPair) {.keyword = "union", .kind = TOKEN_KEYWORD_union },
        (KeywordTokenPair) {.keyword = "Void", .kind = TOKEN_KEYWORD_Void },
        (KeywordTokenPair) {.keyword = "while", .kind = TOKEN_KEYWORD_while },
        (KeywordTokenPair) {.keyword = "xor", .kind = TOKEN_KEYWORD_xor },
    };
    Tokenizer tk; 
    Token result;
    #define ERRMSGLEN 256
    char *errMsg = malloc(ERRMSGLEN);
    for(int i = 0; i < KEYWORDS; i++) {
        tk.input = keywords[i].keyword;
        tk.colNo = 1;
        tk.lineNo = 1;
        result = get_token(&tk);
        memset(errMsg, 0, ERRMSGLEN);
        snprintf(
            errMsg,
            ERRMSGLEN,
            "Wrong token returned for keyword %s. Tokenvalues: expected->%d, actual->%d",
            keywords[i].keyword,
            (int)(keywords[i].kind),
            (int)(result.kind)
        );
        mu_assert(errMsg, result.kind == keywords[i].kind);
    }
    free(errMsg);
    return 0;
}

static char* test_get_token_for_number_literals() {
    Token result;
    Tokenizer tk;
    tk.input = "0b000000___0000001010101_";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Binary int not lexed correctly", result.kind == TOKEN_LITERAL_BIN_INT);

    tk.input = "0xb0A00f0___05040CD187654321_";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Hex int not lexed correctly", result.kind == TOKEN_LITERAL_HEX_INT);

    tk.input = "1_234_567_890";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Dec int not lexed correctly", result.kind == TOKEN_LITERAL_INT);

    tk.input = "1.0";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Float not lexed correctly: 1.0", result.kind == TOKEN_LITERAL_FLOAT);

    tk.input = "1.0e1234567890";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Float not lexed correctly: 1.0e1234567890", result.kind == TOKEN_LITERAL_FLOAT);

    tk.input = "1.0e-1234567890";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Float not lexed correctly: 1.0e-1234567890", result.kind == TOKEN_LITERAL_FLOAT);

    tk.input = "1.";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Should return error for incomplete float: 1.", result.kind == TOKEN_ERROR);

    tk.input = "1.0e";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Should return error for incomplete sci float: 1.0e", result.kind == TOKEN_ERROR);

    tk.input = "1.0e--12";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Should return error for incomplete sci float: 1.0e--12", result.kind == TOKEN_ERROR);
    return 0;
}

static char* test_get_token_for_char_literal() {
    Token result;
    Tokenizer tk;
    tk.input = "'\\\\'";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Double backslash should be char legal liteeral", result.kind == TOKEN_LITERAL_CHAR);

    tk.input = "'\\'";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Single backslash should be error", result.kind == TOKEN_ERROR);

    tk.input = "'\\d'";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Escaping d should be an error token", result.kind == TOKEN_ERROR);

    tk.input = "'a";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Closing single qoute missing should be error token", result.kind == TOKEN_ERROR);

    tk.input = "''";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Empty char should be error", result.kind == TOKEN_ERROR);
    return 0;
}

static char* test_get_token_for_string_literal() {
    Token result;
    Tokenizer tk;
    tk.input = "\"\"";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("Empty string should be string literal", result.kind == TOKEN_LITERAL_STRING);

    tk.input = "\"abcdef\\\"\"";
    tk.colNo = 1;
    tk.lineNo = 1;
    result = get_token(&tk);
    mu_assert("String with escaped quotes should be string literal", result.kind == TOKEN_LITERAL_STRING);
    return 0;
}

char* all_tokenizer_tests() {
    show_testheader("Tokenizer");
    mu_run_test("testing module declaration", test_module_decl_tokens);
    mu_run_test("testing get_token for keywords", test_get_token_for_keywords);
    mu_run_test("testing get_token for number literals", test_get_token_for_number_literals);
    mu_run_test("testing get_token for char literal", test_get_token_for_char_literal);
    mu_run_test("testing get_token for string literal", test_get_token_for_string_literal);
    printf("\n");
    return 0;
}