#include "seidla_tests.h"
#include <stdio.h>

int tests_run = 0;

int main(int argc, char **argv)
{
    char *result = all_tokenizer_tests();
    if(result != 0)
    {
        printf("\033[1;31mError:\033[0m\n");
        printf("\t%s\n", result);
    }
    else
    {
        printf("\e[1;32mALL TESTS PASSED!\e[0m\n\n");
    }
    result = all_primary_expr_parsing_tests();
    if(result != 0)
    {
        printf("\033[1;31mError:\033[0m\n");
        printf("\t%s\n", result);
    }
    else
    {
        printf("\e[1;32mALL TESTS PASSED!\e[0m\n\n");
    }
    result = all_token_context_tests();
    if(result != 0)
    {
        printf("\033[1;31mError:\033[0m\n");
        printf("\t%s\n", result);
    }
    else
    {
        printf("\e[1;32mALL TESTS PASSED!\e[0m\n\n");
    }
    printf("Tests run: %d\n", tests_run);
    return result != 0;
}