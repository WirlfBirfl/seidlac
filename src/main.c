#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "tokenizer.h"

typedef enum {
	EITHER_ERR, EITHER_SUC
} EitherKind;

typedef struct {
	EitherKind kind;
	union {
		char* error;
		void* success;
	} result;
} Either;

Either read_file(char *filePath)
{
    struct stat buffer;
    int statResult = stat(filePath, &buffer);
    size_t n = 0;
    Either result;
    if(statResult == -1)
    {
        result.kind = EITHER_ERR;
        result.result.error = "Could not get file stat.";
        return result;
    }
    FILE *f = fopen(filePath, "r");
    if(f == NULL)
    {
        result.kind = EITHER_ERR;
        result.result.error = "Could not open file.";
        return result;
    }
    char *fileContent = malloc((buffer.st_size + 1 )* sizeof(char));
    int c;
    while((c = fgetc(f)) != EOF)
    {
        fileContent[n++] = (char) c;
    }
    fileContent[n] = EOF;
    result.kind = EITHER_SUC;
    result.result.success = fileContent;
    fclose(f);
    return result;
}

int main(int argc, char** argv) {
	if(argc >= 2) {
		Either res = read_file(argv[1]);
		if(res.kind == EITHER_ERR) {
			printf("%s\n", res.result.error);
			return -1;
		}
        TokenStream stream = tokenizer_tokenize(res.result.success);
	} else {
		printf("%s\n", argv[0]);
	}
	return 0;
}

