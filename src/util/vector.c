#include <stdlib.h>
#include <string.h>

#include "vector.h"

void vector_init(Vector *v, size_t typeSize)
{
    v->size = 0;
    v->typeSize = typeSize;
    v->cap = 10;
    v->data = malloc(v->cap * v->typeSize);
}

void vector_add(Vector *v, void *item)
{
    if(v->size == v->cap)
    {
        v->cap += 10;
        v->data =realloc(v->data, v->cap * v->typeSize);
    }
    memcpy(v->data + v->size * v->typeSize, item, v->typeSize);
    v->size++;
}

void vector_delete(Vector *v, size_t index)
{
    if(index >= v->size)
    {
        return;
    }
    else if(index == v->size - 1)
    {
        v->size--;
        return;
    }
    memmove(v->data + index * v->typeSize, v->data + (index +1) * v->typeSize, (v->size - 1 - index) * v->typeSize);
    v->size--;
}

void* vector_last(Vector *v)
{
    if(v->size == 0)
        return NULL;
    return v->data + (v->size - 1) * v->typeSize;
}

char* vector_get(Vector *v, size_t index)
{
    return v->data + index * v->typeSize;
}

void vector_destroy(Vector *v)
{
    free(v->data);
    free(v);
}

void vector_destroy_items(Vector *v, vector_item_destructor destructor)
{
    for(int i = 0; i<v->size; i++)
    {
        destructor(vector_get(v, i));
    }
    free(v->data);
}

void vector_delete_last(Vector *v)
{
    if(v->size > 0)
    {
        v->size--;
    }
}

void ptrlist_init(PtrList *list)
{
    list->ptrSize = sizeof(char*);
    list->size = 0;
    list->cap = 5;
    list->data = malloc(list->ptrSize * list->cap);
}

void ptrlist_add(PtrList *list, void *data)
{
    if(list->size == list->cap)
    {
        list->cap += 10;
        list->data =realloc(list->data, list->cap * list->ptrSize);
    }
    memcpy(list->data + list->size * list->ptrSize, &data, list->ptrSize);
    list->size++;
}

void* ptrlist_get(PtrList *l, size_t index)
{
    return *(l->data + index * l->ptrSize);
}

void* ptrlist_get_last(PtrList *l)
{
    return *(l->data + (l->size - 1) * l->ptrSize);
}

void* ptrlist_remove(PtrList *l, size_t index)
{
    if(l->size > 0)
    {
        void *item = ptrlist_get(l, index);
        if(index < l->size -1)
        {
            memmove(l->data + index * l->ptrSize, l->data + (index + 1) * l->ptrSize,(l->size - index - 1) * l->ptrSize);
        }
        l->size--;
        return item;
    }
    return NULL;
}

void* ptrlist_remove_last(PtrList *l)
{
    if(l->size > 0)
    {
        void *item = ptrlist_get_last(l);
        l->size--;
        return item; 
    }
    return NULL;
}