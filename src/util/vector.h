#ifndef VECTOR_H
#define VECTOR_H

#include <stdlib.h>
#include <stddef.h>

typedef struct Vector Vector;

typedef struct PtrList PtrList;

typedef void (*vector_item_destructor)(void *);

struct Vector {
    char *data;
    size_t size;
    size_t typeSize;
    size_t cap;
};

struct PtrList {
    void **data;
    size_t size;
    size_t cap;
    size_t ptrSize;
};

void ptrlist_init(PtrList*);

void ptrlist_add(PtrList*, void*);

void* ptrlist_get(PtrList*, size_t);

void* ptrlist_get_last(PtrList*);

void* ptrlist_remove(PtrList*, size_t);

void* ptrlist_remove_last(PtrList*);

void vector_init(Vector*, size_t);

void vector_add(Vector*, void*);

void vector_delete(Vector*, size_t);

char* vector_get(Vector*, size_t);

void* vector_last(Vector *);

void vector_destroy(Vector *);

void vector_destroy_items(Vector *, vector_item_destructor);

void vector_delete_last(Vector *);

#endif