#include "tokenizer.h"

#include <string.h>

int tokenstream_init(TokenStream *stream, size_t capacity) {
    stream->capacity = capacity;
    stream->length = 0;
    stream->tokens = malloc(sizeof(Token) * capacity);
    if(stream->tokens == NULL) {
        return -1;
    }
    return 0;
}

int tokenstream_add(TokenStream *stream, Token *token) {
    if(stream->length == stream->capacity) {
        Token* newTokens = realloc(stream->tokens, stream->capacity * sizeof(Token) * 2);
        if(newTokens == NULL) {
            return -1;
        }
        stream->capacity *= 2;
        stream->tokens = newTokens;
    }
    memcpy(stream->tokens + stream->length, token, sizeof(Token));
    stream->length++;
    return 0;
}

void tokenstream_free(TokenStream *stream) {
    for(size_t i=0; i < stream->length; i++) {
        bs_free(&stream->tokens[i].strVal);
    }
    free(stream->tokens);
    stream->length = 0;
    stream->capacity = 1;
}
