#ifndef SEIDLA_PARSER_H
#define SEIDLA_PARSER_H

#include "tokenizer.h"
#include "ast.h"
#include "util/vector.h"
#include "tokenizer.h"

#define TOKEN_CONTEXT_BITMASK_CHUNK_SIZE 8

typedef struct {
    char bitMask[16];
} TokenContext;

typedef struct {
    Location location;
    char *msg;
} ParseError;

typedef struct {
    TokenStream tokens;
    size_t position;
    Vector parseErrors;
    TokenContext context;
} Parser;

void parser_init(Parser *parser, TokenStream *ts);

Token* parser_peek(Parser *parser);

Token *parser_peek_ahead(Parser *parser, size_t ahead);

void parser_consume_token(Parser *parser);

void parser_report_error(Parser *parser, char *msg);

char parser_is_context_token(Parser *parser, TokenKind kind);

void parser_add_to_context(Parser *parser, TokenKind *tokenKinds, size_t tokenKindc);

void parser_set_context(Parser *parser, TokenContext *newContext);

void parser_swap_context(Parser *parser, TokenContext *newContext);

void token_context_init(TokenContext *context);

void token_context_add_token(TokenContext *tc, TokenKind *kind, size_t n);

char token_context_is_in_context(TokenContext *tc, TokenKind kind);

#endif