#include "ast.h"

DataConstructorExpr *ast_alloc_data_constructor_expr() {
    DataConstructorExpr *dataCons = malloc(sizeof(DataConstructorExpr));
    if(dataCons != NULL) {
        vector_init(&dataCons->exprs, sizeof(Expr));
    }
    return dataCons;
}

ArrayExpr* ast_alloc_array_expr() {
    ArrayExpr *array = malloc(sizeof(ArrayExpr));
    if(array != NULL) {
        vector_init(&array->exprs, sizeof(Expr));
    }
    return array;
}