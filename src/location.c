#include "location.h"

Location location_merge(Location *loc1, Location *loc2) {
    Location result;
    result.startCol = (loc1->startCol < loc2->startCol) ? loc1->startCol : loc2->startCol;
    result.startLine = (loc1->startLine < loc2->startLine) ? loc1->startLine : loc2->startLine;
    result.endCol = (loc1->endCol > loc2->endCol) ? loc1->endCol : loc2->endCol;
    result.endLine = (loc1->endLine > loc2->endLine) ? loc1->endLine : loc2->endLine;
    return result;
}