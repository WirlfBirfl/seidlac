#include "parsing.h"

static StructExpr* parse_struct_expr(Parser*);

Expr* parse_primary_expr(Parser *parser) {
    size_t startIndex = parser->position;
    Expr *result = malloc(sizeof(Expr));
    Token *current = parser_peek(parser);
    if(current->kind == TOKEN_KEYWORD_false) {
        result->child.boolean = 0;
        result->kind = EXPR_LIT_BOOL;
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        parser_consume_token(parser);
    } else if(current->kind == TOKEN_KEYWORD_true) {
        result->child.boolean = 1;
        result->kind = EXPR_LIT_BOOL;
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        parser_consume_token(parser);
    } else if(current->kind == TOKEN_LITERAL_INT) {
        IntExpr *integer = malloc(sizeof(IntExpr));
        integer->kind = INT_DEC;
        integer->bitWwidth = 32;
        integer->value = current->strVal;
        result->child.integer = integer;
        result->kind = EXPR_LIT_INT;
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        parser_consume_token(parser);
    } else if(current->kind == TOKEN_LITERAL_BIN_INT) {
        IntExpr *integer = malloc(sizeof(IntExpr));
        integer->kind = INT_BIN;
        integer->bitWwidth = 32;
        integer->value = current->strVal;
        result->child.integer = integer;
        result->kind = EXPR_LIT_INT;
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        parser_consume_token(parser);
    } else if(current->kind == TOKEN_LITERAL_HEX_INT) {
        IntExpr *integer = malloc(sizeof(IntExpr));
        integer->kind = INT_HEX;
        integer->bitWwidth = 32;
        integer->value = current->strVal;
        result->child.integer = integer;
        result->kind = EXPR_LIT_INT;
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        parser_consume_token(parser);
    } else if(current->kind == TOKEN_LITERAL_FLOAT) {
        FloatExpr *floatExpr = malloc(sizeof(FloatExpr));
        floatExpr->bitWidth = 64;
        floatExpr->value = current->strVal;
        result->kind = EXPR_LIT_FLOAT;
        result->child.real = floatExpr;
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        parser_consume_token(parser);
    } else if(current->kind == TOKEN_LITERAL_CHAR) {
        result->kind = EXPR_LIT_CHAR;
        if(current->strVal.length == 1) {
            result->child.character = current->strVal.buffer[0];
        } else if(current->strVal.length == 2) {
            // TODO: Handle escape chars
        }
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        parser_consume_token(parser);
    } else if(current->kind == TOKEN_LITERAL_STRING) {
        StringExpr *string = malloc(sizeof(StringExpr));
        string->value = current->strVal;
        result->kind = EXPR_LIT_STRING;
        result->child.string = string;
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        parser_consume_token(parser);
    } else if(current->kind == TOKEN_ID) {
        Identifier *id = malloc(sizeof(Identifier));
        id->name = current->strVal;
        result->kind = EXPR_ID;
        result->child.id = id;
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        parser_consume_token(parser);
    } else if(current->kind == TOKEN_OPEN_PAR) {
        parser_consume_token(parser);
        result = parse_expr(parser);
        current = parser_peek(parser);
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        if(current->kind == TOKEN_CLOSE_PAR) {
            parser_consume_token(parser);
        } else {
            result->tokens.endIndex--;
            parser_report_error(parser, "Missing )");
        }
    } else if(current->kind == TOKEN_OPEN_BRA) {
        ArrayExpr *array = ast_alloc_array_expr();
        parser_consume_token(parser);
        while(1) {
            Expr *e = parse_expr(parser);
            vector_add(&array->exprs, e);
            free(e);
            current = parser_peek(parser);
            if(current->kind != TOKEN_COMMA) {
                break;
            } else {
                parser_consume_token(parser);
            }
        }
        result->kind = EXPR_ARRAY;
        result->child.array = array;
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position;
        if(current->kind != TOKEN_CLOSE_BRA) {
            result->tokens.endIndex--;
            parser_report_error(parser, "Missing ]");
        } else {
            parser_consume_token(parser);
        }
    } else if(current->kind == TOKEN_KEYWORD_struct) {
        StructExpr *strucct = parse_struct_expr(parser);
        result->kind = EXPR_STRUCT;
        result->child.strucct = strucct;
        result->tokens.startIndex = startIndex;
        result->tokens.endIndex = parser->position - 1;
    } else if(current->kind == TOKEN_TYPE_ID) {
        if(parser_peek_ahead(parser, 1)->kind == TOKEN_OPEN_CUR) {
            StructExpr *strucct = parse_struct_expr(parser);
            result->kind = EXPR_STRUCT;
            result->child.strucct = strucct;
            result->tokens.startIndex = startIndex;
            result->tokens.endIndex = parser->position - 1;
        } else {
            // data constructor expression
            DataConstructorExpr *dataCons = ast_alloc_data_constructor_expr();
            Identifier typeId;
            typeId.name = current->strVal;
            dataCons->cons = typeId;
            parser_consume_token(parser);
            current = parser_peek(parser);
            if(current->kind == TOKEN_OPERATOR_OR) {
                parser_consume_token(parser);
                current = parser_peek(parser);
            } else {
                parser_report_error(parser, "Missing | after data constructor");
            }
            while(1) {
                Expr *e = parse_expr(parser);
                if(e == NULL) {
                    break;
                }
                vector_add(&dataCons->exprs, e);
                free(e);
                current = parser_peek(parser);
                if(current->kind == TOKEN_OPERATOR_OR) {
                    parser_consume_token(parser);
                    break;
                } else if(current->kind == TOKEN_NEWLINE) {
                    parser_report_error(parser, "Missing termination of data constructor");
                    break;
                }
            }
            result->child.dataConstructor = dataCons;
            result->kind = EXPR_DATA_CONS;
            result->tokens.startIndex = startIndex;
            result->tokens.endIndex = parser->position - 1;
        }
    } else {
        parser_report_error(parser, "Expected primary expression");
        free(result);
        return NULL;
    }
    return result;
}

static StructExpr* parse_struct_expr(Parser *parser) {
    Token *current = parser_peek(parser);
    StructExpr *strucct = malloc(sizeof(StructExpr));
    parser_consume_token(parser);
    current = parser_peek(parser);
    if(current->kind == TOKEN_TYPE_ID) {
        strucct->type.name = current->strVal;
    }
    parser_consume_token(parser);
    current = parser_peek(parser);
    if(current->kind != TOKEN_OPEN_CUR) {
        parser_report_error(parser, "Missing {");
    }
    parser_consume_token(parser);
    // TODO: parse assignments of struct expr
    current = parser_peek(parser);
    if(current->kind != TOKEN_CLOSE_CUR) {
        parser_report_error(parser, "Missing }");
    }
    parser_consume_token(parser);
    return strucct;
}

Expr* parse_postfix_expr(Parser *parser) {
    size_t startIndex = parser->position;
    Expr *result = parse_primary_expr(parser);
    Token *current = parser_peek(parser);
    while(1) {
        switch(current->kind) {
            case TOKEN_OPEN_BRA:
                parser_consume_token(parser);
                Expr *index = parse_primary_expr(parser);
                current = parser_peek(parser);
                SubscriptExpr *subscriptExpr = malloc(sizeof(SubscriptExpr));
                subscriptExpr->index = index;
                subscriptExpr->subscriptee = result;
                if(current->kind == TOKEN_CLOSE_BRA) {
                    parser_consume_token(parser);
                } else {
                    // TODO: Error handling
                }
                result = malloc(sizeof(Expr));
                result->kind = EXPR_SUBSCRIPT;
                result->child.subscript = subscriptExpr;
                result->tokens = subscriptExpr->subscriptee->tokens;
                result->tokens.startIndex = startIndex;
                result->tokens.endIndex = parser->position;
                break;
            case TOKEN_OPEN_PAR:
                break;
            case TOKEN_DOT:
                break;
            case TOKEN_KEYWORD_as:
            default:
                goto postfixdone;
        }
    }
    postfixdone:
    return result;
}

Expr* parse_expr(Parser *parser) {
    return parse_primary_expr(parser);
}
