#include <stdio.h>
#include <string.h>

#include "tokenizer.h"

int is_id_character(char);
int is_id_start_char(char);
int is_type_id_start(char);
int is_type_id_char(char);

static inline void _finish(size_t *length, Token *result, Tokenizer *tokenizer) {
    *length = *length - 1;
    result->loc.startCol = tokenizer->colNo;
    tokenizer->colNo += *length;
    result->loc.endCol = tokenizer->colNo - 1;
    result->loc.startLine = result->loc.endLine = tokenizer->lineNo;
    bs_from_c_str(&result->strVal, tokenizer->input, *length);
    tokenizer->input += *length;
    result->length = *length;
}

static inline void _finish_after_complete(size_t tokenLength, Token *result, Tokenizer *tokenizer) {
    result->loc.startCol = tokenizer->colNo;
    tokenizer->colNo += tokenLength;
    result->loc.endCol = tokenizer->colNo - 1;
    result->loc.startLine = result->loc.endLine = tokenizer->lineNo;
    bs_from_c_str(&result->strVal, tokenizer->input, tokenLength);
    tokenizer->input += tokenLength;
    result->length = tokenLength;
}

int is_id_character(char c) {
    if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_')
        return 1;
    return 0;
}

int is_id_start_char(char c) {
    if((c >= 'a' && c <= 'z') || c == '_')
        return 1;
    return 0;
}

int is_type_id_char(char c) {
    if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_')
        return 1;
    return 0;
}

int is_type_id_start(char c) {
    if((c >= 'A' && c <= 'Z'))
        return 1;
    return 0;
}

int is_hex_digit(char c) {
    return (c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f');
}

int is_escapable_char(char c) {
    return (c == '\\') ||
           (c == '\'') ||
           (c == 't')  ||
           (c == 'b')  ||
           (c == 'n')  ||
           (c == 'r')  ||
           (c == '0')  ||
           (c == 'v');
}

TokenStream tokenizer_tokenize(char *input) {
    Tokenizer tokenizer;
    tokenizer.input = input;
    tokenizer.lineNo = 1;
    tokenizer.colNo = 1;
    TokenStream result;
    if(!tokenstream_init(&result, 10))
        ;
    while(1) {
        Token tmp = get_token(&tokenizer);
        if(tokenstream_add(&result, &tmp))
            break;
        if(tmp.kind == TOKEN_EOF)
            break;
    }
    return result;
}

Token get_token(Tokenizer *tokenizer) {
    Token result;
    bs_empty(&result.strVal);
    while(*tokenizer->input == ' ' || *tokenizer->input == '\t') {
        tokenizer->input++;
        tokenizer->colNo++;
    }
    char c;
    size_t tokenLength = 0;
    c = tokenizer->input[tokenLength++];
    if(c == 'B') goto B;
    if(c == 'F') goto F;
    if(c == 'I') goto I;
    if(c == 'S') goto S;
    if(c == 'U') goto U;
    if(c == 'V') goto V;
    if(c == 'a') goto a;
    if(c == 'b') goto b;
    if(c == 'c') goto c;
    if(c == 'd') goto d;
    if(c == 'e') goto e;
    if(c == 'f') goto f;
    if(c == 'g') goto g;
    if(c == 'i') goto i;
    if(c == 'm') goto m;
    if(c == 'n') goto n;
    if(c == 'o') goto o;
    if(c == 'r') goto r;
    if(c == 's') goto s;
    if(c == 't') goto t;
    if(c == 'u') goto u;
    if(c == 'w') goto w;
    if(c == 'x') goto x;
    if(c == '\'') goto char_lit_start;
    if(c == '"') goto string_lit_start;
    if(c == '0') goto ZERO;
    if(c >= '1' && c <= '9') goto dec_int;
    if(is_id_start_char(c)) goto id;
    if(is_type_id_start(c)) goto typeid;
    if(c == '(') {
        result.kind = TOKEN_OPEN_PAR;
        goto finish_after_complete;
    }
    if(c == ')') {
        result.kind = TOKEN_CLOSE_PAR;
        goto finish_after_complete;
    }
    if(c == '{') {
        result.kind = TOKEN_OPEN_CUR;
        goto finish_after_complete;
    }
    if(c == '}') {
        result.kind = TOKEN_CLOSE_CUR;
        goto finish_after_complete;
    }
    if(c == '[') {
        result.kind = TOKEN_OPEN_BRA;
        goto finish_after_complete;
    }
    if(c == ']') {
        result.kind = TOKEN_CLOSE_BRA;
        goto finish_after_complete;
    }
    if(c == '\n') {
        result.kind = TOKEN_NEWLINE;
        goto finish_after_complete;
    }
    if(c == ',') {
        result.kind = TOKEN_COMMA;
        goto finish_after_complete;
    }
    if(c == ':') {
        result.kind = TOKEN_COLON;
        goto finish_after_complete;
    }
    if(c == ';') {
        result.kind = TOKEN_SEMICOLON;
        goto finish_after_complete;
    }
    if(c == '.') {
        result.kind = TOKEN_DOT;
        goto finish_after_complete;
    }
    if(c == '&') {
        result.kind = TOKEN_OPERATOR_AND;
        goto finish_after_complete;
    }
    if(c == '^') {
        result.kind = TOKEN_OPERATOR_XOR;
        goto finish_after_complete;
    }
    if(c == '|') {
        result.kind = TOKEN_OPERATOR_OR;
        goto finish_after_complete;
    }
    if(c == '>') goto greater_than;
    if(c == '<') goto less_than;
    if(c == '=') goto assignment;
    if(c == '!') goto notequals;
    if(c == '~') {
        result.kind = TOKEN_OPERATOR_NOT;
        goto finish_after_complete;
    }
    if(c == '+') {
        result.kind = TOKEN_OPERATOR_ADD;
        goto finish_after_complete;
    }
    if(c == '-') {
        result.kind = TOKEN_OPERATOR_SUB;
        goto finish_after_complete;
    }
    if(c == '*') {
        result.kind = TOKEN_OPERATOR_MUL;
        goto finish_after_complete;
    }
    if(c == '/') {
        result.kind = TOKEN_OPERATOR_DIV;
        goto finish_after_complete;
    }
    if(c == '%') {
        result.kind = TOKEN_OPERATOR_MOD;
        goto finish_after_complete;
    }
    if(c == EOF) {
        result.kind = TOKEN_EOF;
        goto finish_after_complete;
    }
    result.kind = TOKEN_ERROR;
    goto finish;
    // multi character tokens
    notequals:
        c = tokenizer->input[tokenLength++];
        if(c == '=') {
            result.kind = TOKEN_OPERATOR_NEQ;
            goto finish_after_complete;
        }
        result.kind = TOKEN_ERROR;
        tokenLength--;
        goto finish;
    assignment:
        c = tokenizer->input[tokenLength++];
        if(c == '=') {
            result.kind = TOKEN_OPERATOR_EQU;
            goto finish_after_complete;
        }
        result.kind = TOKEN_OPERATOR_ASSIGNMENT;
        goto finish;
    less_than:
        c = tokenizer->input[tokenLength++];
        if(c == '<') {
            result.kind = TOKEN_OPERATOR_LEFTSHIFT;
            goto finish_after_complete;
        }
        if(c == '=') {
            result.kind = TOKEN_OPERATOR_LTE;
            goto finish_after_complete;
        }
        result.kind = TOKEN_OPERATOR_LT;
        goto finish;
    greater_than:
        c = tokenizer->input[tokenLength++];
        if(c == '>') {
            result.kind = TOKEN_OPERATOR_RIGHTSHIFT;
            goto finish_after_complete;
        }
        if(c == '=') {
            result.kind = TOKEN_OPERATOR_GTE;
            goto finish_after_complete;
        }
        result.kind = TOKEN_OPERATOR_GT;
        goto finish;
    string_lit_start:
        c = tokenizer->input[tokenLength++];
        if(c == '"') {
            result.kind = TOKEN_LITERAL_STRING;
            goto finish_after_complete;
        }
        if(c == '\\') {
            c = tokenizer->input[tokenLength++];
            goto string_lit_start;
        }
        if(c == EOF) {
            result.kind = TOKEN_ERROR;
            goto finish;
        }
        goto string_lit_start;
    char_lit_start:
        c = tokenizer->input[tokenLength++];
        if(c == '\\') goto char_lit_escape;
        if(c >= ' ' && c <= 127) goto char_lit_char_complete;
        result.kind = TOKEN_ERROR;
        goto finish;
    char_lit_escape:
        c = tokenizer->input[tokenLength++];
        if(is_escapable_char(c)) goto char_lit_char_complete;
        result.kind = TOKEN_ERROR;
        goto finish;
    char_lit_char_complete:
        c = tokenizer->input[tokenLength++];
        if(c != '\'') {
            result.kind = TOKEN_ERROR;
            goto finish_after_complete;
        }
        result.kind = TOKEN_LITERAL_CHAR;
        goto finish;
    ZERO:
        c = tokenizer->input[tokenLength++];
        if(c == 'b' || c == 'B') goto bin_int;
        if(c == 'x' || c == 'X') goto hex_int;
        if(c >= '0' && c <= '9') goto dec_int;
        result.kind = TOKEN_LITERAL_INT;
        goto finish;
    bin_int:
        c = tokenizer->input[tokenLength++];
        if(c == '0' || c == '1' || c == '_') goto bin_int;
        if(is_hex_digit(c)) {
            result.kind = TOKEN_ERROR;
            goto finish;
        }
        result.kind = TOKEN_LITERAL_BIN_INT;
        goto finish;
    hex_int:
        c = tokenizer->input[tokenLength++];
        if(is_hex_digit(c) || c == '_') goto hex_int;
        result.kind = TOKEN_LITERAL_HEX_INT;
        goto finish;
    dec_int:
        c = tokenizer->input[tokenLength++];
        if((c >= '0' && c <= '9') || c == '_') goto dec_int;
        if(c == '.') goto half_float;
        if(is_hex_digit(c)) {
            result.kind = TOKEN_ERROR;
            goto finish;
        }
        result.kind = TOKEN_LITERAL_INT;
        goto finish;
    half_float:
        c = tokenizer->input[tokenLength++];
        if(c >= '0' && c <= '9') goto float_;
        result.kind = TOKEN_ERROR;
        goto finish;
    float_:
        c = tokenizer->input[tokenLength++];
        if(c >= '0' && c <= '9') goto float_;
        if(c == 'e' || c == 'E') goto sci_float_incomplete;
        result.kind = TOKEN_LITERAL_FLOAT;
        goto finish;
    sci_float_incomplete:
        c = tokenizer->input[tokenLength++];
        if(c >= '0' && c <= '9') goto sci_float_complete;
        if(c == '-') goto sci_float_neg_exp;
        result.kind = TOKEN_ERROR;
        goto finish;
    sci_float_neg_exp:
        c = tokenizer->input[tokenLength++];
        if(c >= '0' && c <= '9') goto sci_float_complete;
        result.kind = TOKEN_ERROR;
        goto finish;
    sci_float_complete:
        c = tokenizer->input[tokenLength++];
        if(c >= '0' && c <= '9') goto sci_float_complete;
        result.kind = TOKEN_LITERAL_FLOAT;
        goto finish;
    B:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto Bo;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    Bo:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto Boo;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    Boo:
        c = tokenizer->input[tokenLength++];
        if(c == 'l') goto Bool;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    Bool:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_Bool;
        goto finish;
    F:
        c = tokenizer->input[tokenLength++];
        if(c == '3') goto F3;
        if(c == '6') goto F6;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    F3:
        c = tokenizer->input[tokenLength++];
        if(c == '2') goto F32;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    F32:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_F32;
        goto finish;
    F6:
        c = tokenizer->input[tokenLength++];
        if(c == '4') goto F64;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    F64:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_F64;
        goto finish;
    I:
        c = tokenizer->input[tokenLength++];
        if(c == '8') goto I8;
        if(c == '1') goto I1;
        if(c == '3') goto I3;
        if(c == '6') goto I6;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    I8:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_I8;
        goto finish;
    I1:
        c = tokenizer->input[tokenLength++];
        if(c == '6') goto I16;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    I16:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_I16;
        goto finish;
    I3:
        c = tokenizer->input[tokenLength++];
        if(c == '2') goto I32;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    I32:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_I32;
        goto finish;
    I6:
        c = tokenizer->input[tokenLength++];
        if(c == '4') goto I64;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    I64:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_I64;
        goto finish;
    S:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto St;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    St:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto Str;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    Str:
        c = tokenizer->input[tokenLength++];
        if(c == 'i') goto Stri;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    Stri:
        c = tokenizer->input[tokenLength++];
        if(c == 'n') goto Strin;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    Strin:
        c = tokenizer->input[tokenLength++];
        if(c == 'g') goto String;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    String:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_String;
        goto finish;
    U:
        c = tokenizer->input[tokenLength++];
        if(c == '8') goto U8;
        if(c == '1') goto U1;
        if(c == '3') goto U3;
        if(c == '6') goto U6;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    U8:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_U8;
        goto finish;
    U1:
        c = tokenizer->input[tokenLength++];
        if(c == '6') goto U16;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    U16:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_U16;
        goto finish;
    U3:
        c = tokenizer->input[tokenLength++];
        if(c == '2') goto U32;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    U32:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_U32;
        goto finish;
    U6:
        c = tokenizer->input[tokenLength++];
        if(c == '4') goto U64;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    U64:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_U64;
        goto finish;
    V:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto Vo;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    Vo:
        c = tokenizer->input[tokenLength++];
        if(c == 'i') goto Voi;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    Voi:
        c = tokenizer->input[tokenLength++];
        if(c == 'd') goto Void;
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    Void:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_KEYWORD_Void;
        goto finish;
    a:
        c = tokenizer->input[tokenLength++];
        if(c == 'n') goto an;
        if(c == 's') goto as;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    an:
        c = tokenizer->input[tokenLength++];
        if(c == 'd') goto and;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    and:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_and;
        goto finish;
    as:
        c = tokenizer->input[tokenLength++];
        if(c == 'm') goto asm_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_as;
        goto finish;
    asm_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_asm;
        goto finish;
    b:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto br;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    br:
        c = tokenizer->input[tokenLength++];
        if(c == 'e') goto bre;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    bre:
        c = tokenizer->input[tokenLength++];
        if(c == 'a') goto brea;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    brea:
        c = tokenizer->input[tokenLength++];
        if(c == 'k') goto break_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    break_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_break;
        goto finish;
    c:
        c = tokenizer->input[tokenLength++];
        if(c == 'a') goto ca;
        if(c == 'f') goto cf;
        if(c == 'o') goto co;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    ca:
        c = tokenizer->input[tokenLength++];
        if(c == 's') goto cas;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    cas:
        c = tokenizer->input[tokenLength++];
        if(c == 'e') goto case_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    case_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_case;
        goto finish;
    cf:
        c = tokenizer->input[tokenLength++];
        if(c == 'n') goto cfn;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    cfn:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_cfn;
        goto finish;
    co:
        c = tokenizer->input[tokenLength++];
        if(c == 'n') goto con;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    con:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto cont;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    cont:
        c = tokenizer->input[tokenLength++];
        if(c == 'i') goto conti;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    conti:
        c = tokenizer->input[tokenLength++];
        if(c == 'n') goto contin;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    contin:
        c = tokenizer->input[tokenLength++];
        if(c == 'u') goto continu;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    continu:
        c = tokenizer->input[tokenLength++];
        if(c == 'e') goto continue_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    continue_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_continue;
        goto finish;
    d:
        c = tokenizer->input[tokenLength++];
        if(c == 'a') goto da;
        if(c == 'o') goto do_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    da:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto dat;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    dat:
        c = tokenizer->input[tokenLength++];
        if(c == 'a') goto data;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    data:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_data;
        goto finish;
    do_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_do;
        goto finish;
    e:
        c = tokenizer->input[tokenLength++];
        if(c == 'l') goto el;
        if(c == 'r') goto er;
        if(c == 'x') goto ex;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    el:
        c = tokenizer->input[tokenLength++];
        if(c == 'i') goto eli;
        if(c == 's') goto els;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    eli:
        c = tokenizer->input[tokenLength++];
        if(c == 'f') goto elif;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    elif:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_elif;
        goto finish;
    els:
        c = tokenizer->input[tokenLength++];
        if(c == 'e') goto else_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    else_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_else;
        goto finish;
    er:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto err;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    err:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto erro;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    erro:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto error;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    error:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_error;
        goto finish;
    ex:
        c = tokenizer->input[tokenLength++];
        if(c == 'p') goto exp;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    exp:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto expo;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    expo:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto expor;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    expor:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto export;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    export:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_export;
        goto finish;
    f:
        c = tokenizer->input[tokenLength++];
        if(c == 'a') goto fa;
        if(c == 'n') goto fn;
        if(c == 'o') goto fo;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    fa:
        c = tokenizer->input[tokenLength++];
        if(c == 'l') goto fal;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    fal:
        c = tokenizer->input[tokenLength++];
        if(c == 's') goto fals;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    fals:
        c = tokenizer->input[tokenLength++];
        if(c == 'e') goto false;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    false:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_false;
        goto finish;
    fn:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_fn;
        goto finish;
    fo:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto for_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    for_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_for;
        goto finish;
    g:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto go;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    go:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto got;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    got:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto goto_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    goto_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_goto;
        goto finish;
    i:
        c = tokenizer->input[tokenLength++];
        if(c == 'f') goto if_;
        if(c == 'm') goto im;
        if(c == 'n') goto in;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    if_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_if;
        goto finish;
    im:
        c = tokenizer->input[tokenLength++];
        if(c == 'p') goto imp;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    imp:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto impo;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    impo:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto impor;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    impor:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto import;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    import:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_import;
        goto finish;
    in:
        c = tokenizer->input[tokenLength++];
        if(c == 'c') goto inc;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    inc:
        c = tokenizer->input[tokenLength++];
        if(c == 'l') goto incl;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    incl:
        c = tokenizer->input[tokenLength++];
        if(c == 'u') goto inclu;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    inclu:
        c = tokenizer->input[tokenLength++];
        if(c == 'd') goto includ;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    includ:
        c = tokenizer->input[tokenLength++];
        if(c == 'e') goto include;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    include:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_include;
        goto finish;
    m:
        c = tokenizer->input[tokenLength++];
        if(c == 'a') goto ma;
        if(c == 'o') goto mo;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    ma:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto mat;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    mat:
        c = tokenizer->input[tokenLength++];
        if(c == 'c') goto matc;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    matc:
        c = tokenizer->input[tokenLength++];
        if(c == 'h') goto match;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    match:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_match;
        goto finish;
    mo:
        c = tokenizer->input[tokenLength++];
        if(c == 'd') goto mod;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    mod:
        c = tokenizer->input[tokenLength++];
        if(c == 'u') goto modu;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    modu:
        c = tokenizer->input[tokenLength++];
        if(c == 'l') goto modul;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    modul:
        c = tokenizer->input[tokenLength++];
        if(c == 'e') goto module;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    module:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_module;
        goto finish;
    n:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto no;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    no:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto not;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    not:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_not;
        goto finish;
    o:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto or;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    or:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_or;
        goto finish;
    r:
        c = tokenizer->input[tokenLength++];
        if(c == 'e') goto re;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    re:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto ret;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    ret:
        c = tokenizer->input[tokenLength++];
        if(c == 'u') goto retu;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    retu:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto retur;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    retur:
        c = tokenizer->input[tokenLength++];
        if(c == 'n') goto return_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    return_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_return;
        goto finish;
    s:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto st;
        if(c == 'w') goto sw;
        if(c == 'y') goto sy;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    st:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto str;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    str:
        c = tokenizer->input[tokenLength++];
        if(c == 'u') goto stru;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    stru:
        c = tokenizer->input[tokenLength++];
        if(c == 'c') goto struc;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    struc:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto struct_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    struct_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_struct;
        goto finish;
    sw:
        c = tokenizer->input[tokenLength++];
        if(c == 'i') goto swi;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    swi:
        c = tokenizer->input[tokenLength++];
        if(c == 't') goto swit;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    swit:
        c = tokenizer->input[tokenLength++];
        if(c == 'c') goto switc;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    switc:
        c = tokenizer->input[tokenLength++];
        if(c == 'h') goto switch_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    switch_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_switch;
        goto finish;
    sy:
        c = tokenizer->input[tokenLength++];
        if(c == 's') goto sys;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    sys:
        c = tokenizer->input[tokenLength++];
        if(c == 'c') goto sysc;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    sysc:
        c = tokenizer->input[tokenLength++];
        if(c == 'a') goto sysca;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    sysca:
        c = tokenizer->input[tokenLength++];
        if(c == 'l') goto syscal;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    syscal:
        c = tokenizer->input[tokenLength++];
        if(c == 'l') goto syscall;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    syscall:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_syscall;
        goto finish;
    t:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto tr;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    tr:
        c = tokenizer->input[tokenLength++];
        if(c == 'u') goto tru;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    tru:
        c = tokenizer->input[tokenLength++];
        if(c == 'e') goto true;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    true:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_true;
        goto finish;
    u:
        c = tokenizer->input[tokenLength++];
        if(c == 'n') goto un;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    un:
        c = tokenizer->input[tokenLength++];
        if(c == 'i') goto uni;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    uni:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto unio;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    unio:
        c = tokenizer->input[tokenLength++];
        if(c == 'n') goto union_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    union_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_union;
        goto finish;
    w:
        c = tokenizer->input[tokenLength++];
        if(c == 'h') goto wh;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    wh:
        c = tokenizer->input[tokenLength++];
        if(c == 'i') goto whi;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    whi:
        c = tokenizer->input[tokenLength++];
        if(c == 'l') goto whil;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    whil:
        c = tokenizer->input[tokenLength++];
        if(c == 'e') goto while_;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    while_:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_while;
        goto finish;
    x:
        c = tokenizer->input[tokenLength++];
        if(c == 'o') goto xo;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    xo:
        c = tokenizer->input[tokenLength++];
        if(c == 'r') goto xor;
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    xor:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_KEYWORD_xor;
        goto finish;
    id:
        c = tokenizer->input[tokenLength++];
        if(is_id_character(c)) goto id;
        result.kind = TOKEN_ID;
        goto finish;
    typeid:
        c = tokenizer->input[tokenLength++];
        if(is_type_id_char(c)) goto typeid;
        result.kind = TOKEN_TYPE_ID;
        goto finish;
    finish:
    _finish(&tokenLength, &result, tokenizer);
    goto final;
    finish_after_complete:
    _finish_after_complete(tokenLength, &result, tokenizer);
    final:
    return result;
}