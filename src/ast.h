#ifndef SEIDLA_AST_H
#define SEIDLA_AST_H

#include <stdlib.h>
#include "tokenizer.h"
#include "buffered_string.h"
#include "util/vector.h"

typedef struct {
    size_t length;
    Token *tokens;
} TokenSlice;

typedef struct TokenRange {
    size_t startIndex;
    size_t endIndex;
} TokenRange;

typedef struct Expr Expr;

/*
* Primary expression typedefs
*/
typedef struct {
    BufferedString name;
} Identifier;
typedef enum {
    INT_HEX, INT_DEC, INT_BIN 
} IntKind;

typedef struct {
    size_t bitWwidth;
    IntKind kind;
    BufferedString value;
} IntExpr;

typedef struct {
    size_t bitWidth;
    BufferedString value;
} FloatExpr;

typedef struct {
    BufferedString value;
} StringExpr;

typedef struct {
    Vector exprs;
} ArrayExpr;

typedef struct {
    // TODO: Type id must be parsed here
    Identifier type;
    // TODO: Assignemnts
    void *assignments;
} StructExpr;

typedef struct {
    Identifier cons;
    Vector exprs;
} DataConstructorExpr;

/**
 * Postfix expressions 
 */

typedef struct {
    Expr *subscriptee;
    Expr *index;
} SubscriptExpr;

typedef enum {
    EXPR_ID,
    EXPR_LIT_INT, EXPR_LIT_FLOAT, EXPR_LIT_CHAR, EXPR_LIT_STRING, EXPR_LIT_BOOL,
    EXPR_LITERAL, EXPR_ARRAY, EXPR_STRUCT, EXPR_DATA_CONS,
    EXPR_CALL, EXPR_MEMBER, EXPR_SUBSCRIPT, EXPR_TYPE_CAST
} ExprKind;

typedef struct Expr {
    ExprKind kind;
    union {
        Identifier *id;
        // Literal expressions
        char character;
        char boolean;
        IntExpr *integer;
        FloatExpr *real;
        StringExpr *string;
        ArrayExpr *array;
        StructExpr *strucct;
        DataConstructorExpr *dataConstructor;
        SubscriptExpr *subscript;
    } child;
    TokenRange tokens;
} Expr;


DataConstructorExpr *ast_alloc_data_constructor_expr();
ArrayExpr* ast_alloc_array_expr();

#endif