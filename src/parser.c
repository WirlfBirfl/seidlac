#include "parser.h"

#include <string.h>

void parser_init(Parser *parser, TokenStream *ts) {
    parser->tokens = *ts;
    vector_init(&parser->parseErrors, sizeof(ParseError));
    parser->position = 0;
    token_context_init(&parser->context);
}

Token* parser_peek(Parser *parser) {
    return &parser->tokens.tokens[parser->position];
}

void parser_consume_token(Parser *parser) {
    if(parser->position < parser->tokens.length - 1)
        parser->position++;
}

void parser_report_error(Parser *parser, char *msg) {
    ParseError error;
    error.location = parser_peek(parser)->loc;
    error.msg = msg;
    vector_add(&parser->parseErrors, &error);
}

Token *parser_peek_ahead(Parser *parser, size_t ahead) {
    if(parser->position + ahead < parser->tokens.length) {
        return &parser->tokens.tokens[parser->position + ahead];
    }
    return &parser->tokens.tokens[parser->tokens.length - 1];
}

Token* get_current_token(Parser *parser) {
    return &parser->tokens.tokens[parser->position];
}

void consume_token(Parser *parser) {
    parser->position++;
}

char parser_is_context_token(Parser *parser, TokenKind kind) {
    return token_context_is_in_context(&parser->context, kind);
}

void parser_add_to_context(Parser *parser, TokenKind *tokenKinds, size_t tokenKindc) {
    token_context_add_token(&parser->context, tokenKinds, tokenKindc);
}

void parser_set_context(Parser *parser, TokenContext *newContext) {
    memcpy(&parser->context, newContext, sizeof(TokenContext));
}

void parser_swap_context(Parser *parser, TokenContext *newContext) {
    TokenContext tc;
    memcpy(&tc, &parser->context, sizeof(TokenContext));
    memcpy(&parser->context, newContext, sizeof(TokenContext));
    memcpy(newContext, &tc, sizeof(TokenContext));
}

void token_context_add_token(TokenContext *tc, TokenKind *tokenKinds, size_t n) {
    for(size_t i = 0; i < n; i++) {
        int ordinal = (int)tokenKinds[i];
        int byteOffset = ordinal / TOKEN_CONTEXT_BITMASK_CHUNK_SIZE;
        int bitOffset = ordinal % TOKEN_CONTEXT_BITMASK_CHUNK_SIZE;
        tc->bitMask[byteOffset] |= 1 << bitOffset; 
    }
}

char token_context_is_in_context(TokenContext *tc, TokenKind kind) {
    int ordinal = (int)kind;
    int byteOffset = ordinal / TOKEN_CONTEXT_BITMASK_CHUNK_SIZE;
    int bitOffset = ordinal % TOKEN_CONTEXT_BITMASK_CHUNK_SIZE;
    return (tc->bitMask[byteOffset] >> bitOffset) & 1;
}

void token_context_init(TokenContext *context) {
    memset(context, 0, sizeof(TokenContext));
}