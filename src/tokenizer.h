#ifndef TOKENIZER_H
#define TOKENIZER_H

#include <stdlib.h>

#include "buffered_string.h"
#include "location.h"

typedef enum {
    // Keywords
    TOKEN_KEYWORD_and,
    TOKEN_KEYWORD_as,
    TOKEN_KEYWORD_asm,
    TOKEN_KEYWORD_Bool,
    TOKEN_KEYWORD_break,
    TOKEN_KEYWORD_case,
    TOKEN_KEYWORD_cfn,
    TOKEN_KEYWORD_continue,
    TOKEN_KEYWORD_data,
    TOKEN_KEYWORD_do,
    TOKEN_KEYWORD_elif,
    TOKEN_KEYWORD_else,
    TOKEN_KEYWORD_error,
    TOKEN_KEYWORD_export,
    TOKEN_KEYWORD_F32,
    TOKEN_KEYWORD_F64,
    TOKEN_KEYWORD_false,
    TOKEN_KEYWORD_fn,
    TOKEN_KEYWORD_for,
    TOKEN_KEYWORD_goto,
    TOKEN_KEYWORD_I8,
    TOKEN_KEYWORD_I16,
    TOKEN_KEYWORD_I32,
    TOKEN_KEYWORD_I64,
    TOKEN_KEYWORD_if,
    TOKEN_KEYWORD_import,
    TOKEN_KEYWORD_include,
    TOKEN_KEYWORD_match,
    TOKEN_KEYWORD_module,
    TOKEN_KEYWORD_not,
    TOKEN_KEYWORD_or,
    TOKEN_KEYWORD_return,
    TOKEN_KEYWORD_String,
    TOKEN_KEYWORD_struct,
    TOKEN_KEYWORD_switch,
    TOKEN_KEYWORD_syscall,
    TOKEN_KEYWORD_true,
    TOKEN_KEYWORD_U8,
    TOKEN_KEYWORD_U16,
    TOKEN_KEYWORD_U32,
    TOKEN_KEYWORD_U64,
    TOKEN_KEYWORD_union,
    TOKEN_KEYWORD_Void,
    TOKEN_KEYWORD_while,
    TOKEN_KEYWORD_xor,
    // punctuation
    TOKEN_OPEN_PAR,
    TOKEN_CLOSE_PAR,
    TOKEN_OPEN_CUR,
    TOKEN_CLOSE_CUR,
    TOKEN_OPEN_BRA,
    TOKEN_CLOSE_BRA,
    TOKEN_SEMICOLON,
    TOKEN_COLON,
    TOKEN_COMMA,
    TOKEN_DOT,
    // operators
    TOKEN_OPERATOR_ASSIGNMENT,
    // comparrison
    TOKEN_OPERATOR_EQU,
    TOKEN_OPERATOR_NEQ,
    TOKEN_OPERATOR_LT,
    TOKEN_OPERATOR_LTE,
    TOKEN_OPERATOR_GT,
    TOKEN_OPERATOR_GTE,
    // arithmetic
    TOKEN_OPERATOR_ADD,
    TOKEN_OPERATOR_SUB,
    TOKEN_OPERATOR_MUL,
    TOKEN_OPERATOR_DIV,
    TOKEN_OPERATOR_MOD,
    // bitwise
    TOKEN_OPERATOR_LEFTSHIFT,
    TOKEN_OPERATOR_RIGHTSHIFT,
    TOKEN_OPERATOR_NOT,
    TOKEN_OPERATOR_XOR,
    TOKEN_OPERATOR_AND,
    TOKEN_OPERATOR_OR,
    // literals 
    TOKEN_LITERAL_INT,
    TOKEN_LITERAL_BIN_INT,
    TOKEN_LITERAL_HEX_INT,
    TOKEN_LITERAL_FLOAT,
    TOKEN_LITERAL_CHAR,
    TOKEN_LITERAL_STRING,
    // ids
    TOKEN_ID, 
    TOKEN_TYPE_ID, 
    // other
    TOKEN_NEWLINE, 
    TOKEN_EOF, 
    TOKEN_ERROR
} TokenKind;

typedef struct {
    TokenKind kind;
    BufferedString strVal;
    Location loc;
    size_t length;
} Token;

typedef struct {
    char *input;
    size_t lineNo;
    size_t colNo;
} Tokenizer;

typedef struct {
    size_t length;
    size_t capacity;
    Token *tokens;
} TokenStream;

TokenStream tokenizer_tokenize(char *input);
Token get_token(Tokenizer *);

int tokenstream_init(TokenStream *stream, size_t capacity);

int tokenstream_add(TokenStream *stream, Token *token);

void tokenstream_free(TokenStream *stream);

#endif