#ifndef SEIDLA_PARSING
#define SEIDLA_PARSING

#include "ast.h"
#include "parser.h"

Expr* parse_expr(Parser *parser);

Expr* parse_postfix_expr(Parser *parser);

Expr* parse_primary_expr(Parser *parser);

#endif