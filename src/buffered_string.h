#ifndef BUFFERED_STRING_H
#define BUFFERED_STRING_H

#include <stdlib.h>

#ifndef STRING_BUFFER_SIZE
#define STRING_BUFFER_SIZE 16
#endif

typedef struct {
    char initialized;
    char buffer[STRING_BUFFER_SIZE];
    size_t length;
    size_t capacity;
    char *str;
} BufferedString;

int bs_from_c_str(BufferedString *bs, char *str, size_t n);

int bs_cmp_c_str(BufferedString *bs, char *other);

void bs_free(BufferedString *bs);

void bs_empty(BufferedString *bs);

#endif