#include <stdlib.h>
#include <string.h>

#include "buffered_string.h"

int bs_from_c_str(BufferedString *bs, char *str, size_t length) {
    bs->initialized = 1;
    bs->capacity = 0;
    bs->str = NULL;
    memset(bs->buffer, 0, STRING_BUFFER_SIZE);
    if(length > STRING_BUFFER_SIZE) {
        bs->str = (char*)malloc(length);
        if(bs->str == NULL) {
            return -1;
        }
        bs->capacity = length * 2;
        memcpy(bs->str, str, length);
    } else {
        memcpy(bs->buffer, str, length);
    }
    bs->length = length;
    return 0;
}

int bs_cmp_c_str(BufferedString *bs, char *other) {
    if(!bs->initialized) 
        return -1;
    size_t otherLength = strlen(other);
    if(otherLength != bs->length)
        return -1;
    if(bs->length > STRING_BUFFER_SIZE) {
        return strncmp(bs->str, other, otherLength);
    } else {
        return strncmp(bs->buffer, other, otherLength);
    }
}

void bs_empty(BufferedString *bs) {
    bs->length = 0;
    bs->capacity = 0;
    bs->str = NULL;
    bs->initialized = 1;
    memset(bs->buffer, 0, STRING_BUFFER_SIZE);
}

void bs_free(BufferedString *bs) {
    bs->initialized = 0;
    if(bs->length > STRING_BUFFER_SIZE)
        free(bs->str);
    bs->length = 0;
}