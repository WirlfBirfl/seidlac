#ifndef SEIDLA_LOCATION
#define SEIDLA_LOCATION

#include <stdlib.h>

typedef struct {
    size_t startCol;
    size_t endCol;
    size_t startLine;
    size_t endLine;
} Location;

Location location_merge(Location *loc1, Location *loc2);

#endif